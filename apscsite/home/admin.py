from django.contrib import admin


from wagtail.contrib.modeladmin.options import (
    ModelAdmin,
    modeladmin_register,
)

from .models import BlogPage, GaleriaPage, StaticPage

from blog.models import BlogCategory

# Register your models here.

class BlogAdmin (ModelAdmin):
    """ BLOG MODEL"""
    model= BlogPage
    menu_label="Posts"
    menu_icon="openquote"
    menu_order= 300
    add_to_settings_menu=False
    exclude_from_explorer= True
    list_display = ("title","date")
    search_field= ("title","intro")

modeladmin_register(BlogAdmin)

class StaticAdmin (ModelAdmin):
    """ STATIC PAGES MODEL"""
    model= StaticPage
    menu_label="Páginas Estáticas"
    menu_icon="doc-empty"
    menu_order= 200
    add_to_settings_menu=False
    exclude_from_explorer= False
    list_display = ("title",)
    search_field= ("title","intro")

modeladmin_register(StaticAdmin)

class GaleriasAdmin (ModelAdmin):
    """ Galeria MODEL"""
    model= GaleriaPage
    menu_label="Galerias"
    menu_icon="image"
    menu_order= 300
    add_to_settings_menu=False
    exclude_from_explorer= False
    list_display = ("title","date")
    search_field= ("title","intro")

modeladmin_register(GaleriasAdmin)

class CategoryAdmin (ModelAdmin):
    """ BLOG MODEL"""
    model= BlogCategory
    menu_label="Categorias"
    menu_icon="folder"
    menu_order= 300
    add_to_settings_menu=False
    exclude_from_explorer= True
    list_display = ("name",)
    search_field= ("name",)

modeladmin_register(CategoryAdmin)