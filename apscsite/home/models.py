from django import forms
from django.db import models

from modelcluster.fields import ParentalKey, ParentalManyToManyField
from modelcluster.contrib.taggit import ClusterTaggableManager
from taggit.models import TaggedItemBase

from wagtail.models import Page, Orderable
from wagtail.fields import StreamField
from wagtail.fields import RichTextField
from wagtail import blocks
from wagtail.admin.panels import FieldPanel, InlinePanel, MultiFieldPanel
from wagtail.images.blocks import ImageChooserBlock
from wagtail.search import index


class HomePage(Page):
    body = RichTextField(blank=True)

    # get recent blogs (wagtail has by default first_published_at field)
    def get_recent_blogs(self):
        max_count = 5 # max count for displaying post
        return BlogPage.objects.all().order_by('-first_published_at')[:max_count]

    # add this to custom context 
    def get_context(self, request):
        context = super(HomePage, self).get_context(request)
        context['blogs'] = self.get_recent_blogs()
        return context
        content_panels = Page.content_panels + [
            FieldPanel('body', classname="full"),
        ]

class EstaticasIndexPage(Page):
    intro = RichTextField(blank=True)
    max_count= 1
    subpage_types= ['StaticPage']
    content_panels = Page.content_panels + [
        FieldPanel('intro', classname="full")
    ]
    class Meta:
        verbose_name = 'Diretório Estatico'
        verbose_name_plural = 'Diretórios Estáticos'

class StaticPage(Page):
    parent_page_types = ['EstaticasIndexPage']
    def get_template(self, request, *args, **kwargs):
        titulo = self.title
        slugg= self.slug
        templink = 'estaticas/'+ slugg +'.html'
        return templink
    class Meta:
        verbose_name = 'Página Estatica'
        verbose_name_plural = 'Páginas Estáticas'
    

class BlogPageTag(TaggedItemBase):
    content_object = ParentalKey(
        'BlogPage',
        related_name='tagged_items',
        on_delete=models.CASCADE
    )

    
class BlogTagIndexPage(Page):

    def get_context(self, request):

        # Filter by tag
        tag = request.GET.get('tag')
        blogpages = BlogPage.objects.filter(tags__name=tag)

        # Update template context
        context = super().get_context(request)
        context['blogpages'] = blogpages
        return context

    

class PostsPage(Page):
    intro = RichTextField(blank=True)
    max_count= 1
    subpage_types= ['BlogPage']
    content_panels = Page.content_panels + [
        FieldPanel('intro', classname="full")
    ]
    
#Pagina com todas as galerias    
class GaleriasIndexPage(Page):
    subpage_types= ['GaleriaPage']
    intro = RichTextField(blank=True)
    max_count= 1
    content_panels = Page.content_panels + [
        FieldPanel('intro', classname="full")
    ]

#Galeria Isolada
class GaleriaPage(Page):
    date = models.DateField("Post date")
    intro = models.CharField(max_length=250)
    parent_page_types = ['GaleriasIndexPage']
    def main_image(self):
        gallery_item = self.gallery_images.first()
        if gallery_item:
            return gallery_item.image
        else:
            return None

    search_fields = Page.search_fields + [
        index.SearchField('intro'),
    ]
    class Meta:
        verbose_name = 'Galeria'
        verbose_name_plural = 'Galerias'
    

    content_panels = Page.content_panels + [
        MultiFieldPanel([
            FieldPanel('date'),
        ], heading="Galeria de Imagens"),
        FieldPanel('intro'),
        InlinePanel('gallery_images', label="Imagens de Galeria"),
    ]


class BlogPage(Page):
    date = models.DateField("Post date")
    intro = models.CharField(max_length=250)
    body = RichTextField(blank=True)
    tags = ClusterTaggableManager(through=BlogPageTag, blank=True)
    categories = ParentalManyToManyField('blog.BlogCategory', blank=True)
    parent_page_types = ['PostsPage']
    def main_image(self):
        gallery_item = self.gallery_images.first()
        if gallery_item:
            return gallery_item.image
        else:
            return None

    search_fields = Page.search_fields + [
        index.SearchField('intro'),
        index.SearchField('body'),
    ]

    content_panels = Page.content_panels + [
        MultiFieldPanel([
            FieldPanel('date'),
            FieldPanel('tags'),
            FieldPanel('categories', widget=forms.CheckboxSelectMultiple),
        ], heading="Blog information"),
        FieldPanel('intro'),
        FieldPanel('body'),
        InlinePanel('gallery_images', label="Gallery images"),
    ]

    def relacionados(self):
        relacionados = Page.objects.sibling_of(self, inclusive=False)
        return relacionados

class BlogPageGalleryImage(Orderable):
    page = ParentalKey(BlogPage, on_delete=models.CASCADE, related_name='gallery_images')
    image = models.ForeignKey(
        'wagtailimages.Image', on_delete=models.CASCADE, related_name='+'
    )
    caption = models.CharField(blank=True, max_length=250)

    panels = [
        FieldPanel('image'),
        FieldPanel('caption'),
    ]


class GaleriaPageGalleryImage(Orderable):
    page = ParentalKey(GaleriaPage, on_delete=models.CASCADE, related_name='gallery_images')
    image = models.ForeignKey(
        'wagtailimages.Image', on_delete=models.CASCADE, related_name='+'
    )
    caption = models.CharField(blank=True, max_length=250)

    panels = [
        FieldPanel('image'),
        FieldPanel('caption'),
    ]
    

#Biblioteca
# Páginas estatica, com links para paginas de cada tipo de bliblioteca
# Links para indexpages de cada tipo de item de biblioteca: Vídeos, Livros e textos

#Indexes de bibliotecas
class VideosLibPage(Page):
    subpage_types= ['VideoPage']
    intro = RichTextField(blank=True)
    max_count= 1
    content_panels = Page.content_panels + [
        FieldPanel('intro', classname="full")
    ]
    class Meta:
        verbose_name = 'Biblioteca de Vídeo'
        verbose_name_plural = 'Bibliotecas de Vídeo'
class LivrosLibPage(Page):
    subpage_types= ['LivroPage']
    intro = RichTextField(blank=True)
    max_count= 1
    content_panels = Page.content_panels + [
        FieldPanel('intro', classname="full")
    ]
    class Meta:
        verbose_name = 'Biblioteca de Livro'
        verbose_name_plural = 'Bibliotecas de Livros'
class TextosLibPage(Page):
    subpage_types= ['TextoPage']
    intro = RichTextField(blank=True)
    max_count= 1
    content_panels = Page.content_panels + [
        FieldPanel('intro', classname="full")
    ]

#Models de itens das bibliotecas

class VideoPage(Page):
    parent_page_types = ['VideosLibPage']
    intro = models.CharField(blank=True, max_length=250, null= True)
    videoid= models.CharField(blank=True, max_length=250, null= True)
    body = models.CharField(blank=True, max_length=800, null= True)
    # videoid= StreamField([
    #     ('youtubeid', blocks.CharBlock(required=False)),
    #     ('thumbnail', ImageChooserBlock(required=False)),
    # ], use_json_field=True, null=True, blank=True,) 
    imagex = models.ForeignKey(
        'wagtailimages.Image', on_delete=models.CASCADE, related_name='+', null=True
    )

    
    class Meta:
        verbose_name = 'Vídeo'
        verbose_name_plural = 'Vídeos'

    content_panels = [
        FieldPanel('title', heading='Título'),
        FieldPanel('intro',heading='Resumo'),
        FieldPanel('videoid', heading='Id do Vídeo do Youtube'),
        FieldPanel('body', heading='Descrição'),
        FieldPanel('imagex', heading='Imagem de miniatura')
    ]

class LivroPage(Page):
    parent_page_types = ['LivrosLibPage']
    intro = models.CharField(blank=True, max_length=250, null= True)
    body = models.CharField(blank=True, max_length=800, null= True)
    imagex = models.ForeignKey(
        'wagtailimages.Image', on_delete=models.CASCADE, related_name='+', null=True
    )
    linklivro = models.URLField(max_length=256, null=True)

    
    class Meta:
        verbose_name = 'Livro'
        verbose_name_plural = 'Livros'

    content_panels = [
        FieldPanel('title', heading='Título'),
        FieldPanel('intro',heading='Resumo'),
        FieldPanel('linklivro', heading='Link do Livro'),
        FieldPanel('body', heading='Descrição'),
        FieldPanel('imagex', heading='Imagem de miniatura')
    ]

class TextoPage(Page):
    parent_page_types = ['TextosLibPage']
    intro = models.CharField(blank=True, max_length=250, null= True)
    body = models.CharField(blank=True, max_length=800, null= True)
    imagex = models.ForeignKey(
        'wagtailimages.Image', on_delete=models.CASCADE, related_name='+', null=True
    )
    linktexto = models.URLField(max_length=256, null=True)

    
    class Meta:
        verbose_name = 'Texto'
        verbose_name_plural = 'Textos'

    content_panels = [
        FieldPanel('title', heading='Título'),
        FieldPanel('intro',heading='Resumo'),
        FieldPanel('linktexto', heading='Link do Texto'),
        FieldPanel('body', heading='Descrição'),
        FieldPanel('imagex', heading='Imagem de miniatura')
    ]