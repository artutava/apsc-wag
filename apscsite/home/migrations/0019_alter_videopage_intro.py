# Generated by Django 4.0.7 on 2022-11-24 14:49

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('home', '0018_alter_videopage_videoid'),
    ]

    operations = [
        migrations.AlterField(
            model_name='videopage',
            name='intro',
            field=models.CharField(blank=True, max_length=250, null=True),
        ),
    ]
